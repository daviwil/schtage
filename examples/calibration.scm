(slides
 (title "Calibrating Text Sizes in Generated Slides"
        #:author "David Wilson"
        #:organization "System Crafters"
        #:url '(a (@ (href "https://systemcrafters.net")) "systemcrafters.net")
        #:event "Hacking at Home")

 (section "part 1"
          "coffee is good ☕")

 "XXX"
 "XXXXX"
 "XXXXXXXXXX"
 "XXXXXXXXXXXXXXX"
 "XXXXXXXXXXXXXXXXXXXX"
 "XXXXXXXXXXXXXXXXXX XXXXXX"

 (list "testing"
       "list"
       "entries")

 (bullets "cool"
          "fun"
          "yes")

 (text "done."))

(slides
 (title "Cultivating a Welcoming Free Software Community That Lasts"
        #:author "David Wilson"
        #:organization "System Crafters"
        #:url '(a (@ (href "https://systemcrafters.net")) "systemcrafters.net")
        #:event "LibrePlanet 2024")

 "1. why community matters"
 "2. cultivating community"
 "3. igniting collaboration"
 "4. the long term"

 (section "part 1"
          "why community matters 💡")

 "40 YEARS\n😱"
 "GNU" ;; use the logo?

 (bullets "inspect and change the program"
          "share the program")

 "the mission"

 (bullets "planned obsolesence"
          "corporate surveillance"
          "data breaches")

 "the power\nof community"

 (list "a place to call home"

       "comfortable"
       "welcomed\nand appreciated"
       "getting help"
       "inspiration"
       "working together"

       "find their own place")

 (list "an engine for growth"

       "not about numbers"
       "real impact"

       "fuel for progress"
       "tell other people"
       "help each other"

       "marketing"
       "sharing what\nthey learn")

 (list "an incubator\nfor contributors"

       "beyond basic\nparticipation"

       "big goals"
       "replace the\nproprietary"
       "many people\nworking together"
       "build a\nhealthy\ncommunity"

       (bullets "1. works"
                "2. enjoyable"
                "3. good replacement")

       "motivating\nnew members"
       "the path for new\ncontributors"
       "make it easy!")

 (list "what can\nwe do?"
       "welcoming to all"
       "encouraged\nto contribute"
       "maintain culture\nand values")

 (section "part 2"
          "cultivating community 🌳")

 "they must\nfeel welcome!"

 (list "what is\nwelcoming?"

       "Foo"
       "Bar"

       "which is welcoming?"

       "tone matters."
       "people notice!")

 (list "you set\nthe tone"

       "positive\nhelpful\nencouraging"
       "critical\ndiscriminating"

       "public\nconduct\nmatters"

       "nobody is\nperfect"
       "be authentic"
       "how you present\nyourself"

       "you're a\nrole model"
       "set the\nright example"

       "attract\ngreat\npeople"
       "it's rare!")

 (list "preserve\nthe vibe"

       "hard profanity"
       "inappropriate\njokes"
       "teasing of others"
       "negative\ncommunication"

       "it will\nget worse"
       "\"those\"\ncommunities"

       "what do\nyou do?"
       "gentle\nreminders"
       "use humor"

       "be (slightly)\nflexible"
       "extinguish\nthe spread"

       "encourage\npositive\nspeech"
       "you'll know"
       "bye trolls\n👿")

 (list "you need\na big sign"

       "huge\nbillboard"
       "your website"

       "invite people\nto join you"
       "what you're\nall about"
       "pointers to projects"
       "how to help"
       "great opportunity\nto welcome them")

 (section "part 3"
          "igniting collaboration 🔥")

 (list "a focal point"
       "create easy\nopporunities"
       "one\nsmall\ncontribution"

       "how?"

       "identify\nstarter tasks"
       "offer your help"

       "documentation\nand translation"

       "improve\ncontributing\ndocs"

       "artwork\n  web design  \ntutorials"

       "a personal\ninvestment"

       "become\ntheir first\ncontribution")

 (list "use welcoming tools"

       "sending patch files"
       "pull requests"

       "don't add friction!"

       "use FOSS forges\nlike Forgejo"

       "just click edit"
       "merges are\nmotivating!"

       "don't use\nslack or discord"
       "use matrix instead"
       "IRC?"

       "discourse forums"

       "wikis for reference"

       "choose tools\nthat respect\nuser freedom"
       "find the best balance\nfor everyone!")

 (list "show enthusiasm\nfor new ideas"

       "enthusiasm is\nmotivating"

       "publicly express\ngratitude"

       "this is welcoming, too!"

       "promote\ncommunity\nprojects"

       "craftering\nby Shom"

       "provide community news")

 (list "craft a\ncompelling\nmission"
       "path toward a\nbetter future"
       "gives purpose"
       "it may not be\nclear at first")

 (section "part 4"
          "the long term 🌅")

 (list "engaged contributors"
       "promote helpful people"
       "it shows appreciation"

       (bullets "help without asking"
                "help others"
                "invested")

       "those you promote\ncan promote others"

       "crafted emacs\njeff bowman"

       "encourage promoting\nnew people"

       "create a\npyramid scheme"

       "beware\nsupply chain\nattacks"

       "responsibility early\ntrust is earned\nover time")

 (list "document culture\nand values"

       "not too formal"

       "focus on the positive"

       "inspire with the mission")

 (list "make yourself\nreplaceable"
       "collective activity"
       "you can't\ndo everything!"
       "make the work clear"
       "document everything")

 (list "be adaptable\nto change"
       "listen to your\ncommunity")

 (list "why\nfree software\nexists"
       "it's about the people"
       "the community is\nmost important"
       "act with the\ncommunity\nin mind"
       "go forth\ncultivate community\nand welcome everyone!"))

(define-module (schtage)
  #:use-module (sxml simple)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (ice-9 regex)
  #:export (title
            text
            bullets
            section
            slides
            slides->html))

(define (sanitize-hint-text hint-text)
  (let ((match (string-match "[^ -~•]+" hint-text)))
    (if match
        (regexp-substitute #f match 'pre " " 'post)
        hint-text)))

(define* (make-slide contents
                     #:key
                     (type "text")
                     (hint-text ""))
  (lambda (next-slide-hint)
    (values `(div (@ (class "background"))
                  (div (@ (class "slide-container"))
                       (div (@ (class ,(format #f "slide ~a" type)))
                            ,@contents)
                       (div (@ (class "hint-container"))
                            (div (@ (class "hint-text"))
                                 ;; Strip emojis or newlines from the hint text
                                 ,(sanitize-hint-text next-slide-hint)))))
            hint-text)))

(define* (title title-str
                #:key
                (subtitle "")
                (author "")
                (organization "")
                (url "")
                (event ""))
  (make-slide `((div (@ (class "title-lines"))
                     (div (@ (class "title-text")) ,title-str)
                     (div (@ (class "subtitle")) ,subtitle))
                (div (@ (class "title-details"))
                     (div ,author)
                     (div ,organization)
                     (div (a (@ (href ,url)) ,url))
                     (div (@ (class "event-text")) ,event)))
              #:type "title"))

(define (get-size-class str)
  (let* ((lines (string-split str #\newline))
         (len (fold (lambda (str longest-len)
                      (let ((len (string-length str)))
                        (if (> len longest-len)
                            len
                            longest-len)))
                    0
                    lines)))
    (cond
     ((<= len 5) "text-5")
     ((<= len 10) "text-10")
     ((<= len 15) "text-15")
     ((<= len 20) "text-20")
     (else "text-long"))))

(define* (text str #:key color hint-text)
  (make-slide (reverse
               (fold (lambda (part lines)
                       (if (null? lines)
                           (cons part lines)
                           (cons part (cons '(br) lines))))
                     '()
                     (string-split str #\newline)))
              #:type (format #f
                             "text ~a"
                             (get-size-class str))
              #:hint-text (or hint-text str)))

(define* (bullets #:key (expand? #t) #:rest items)
  (let ((slide-lines '()))
    (fold (lambda (item slides)
            (set! slide-lines
                  (append slide-lines `((li ,item))))
            (append slides
                    (list (make-slide `((ul ,slide-lines))
                                      #:type "bullets"
                                      #:hint-text (format #f "• ~a\n" item)))))
          '()
          items)))

(define* (section title #:optional subtitle)
  (make-slide `((div (@ (class "section-title")) ,title)
                (div (@ (class "section-subtitle")) ,subtitle))
              #:type "section"
              #:hint-text (format #f "~a: ~a" title subtitle)))

(define (slides->string slide-list)
  (string-join (map (lambda (slide)
                      (if (pair? slide)
                          (slides->string slide)
                          slide))
                    slide-list)
               "\n\n---\n\n"))

(define (slide->sxml slide hint-text)
  (cond
   ((string? slide)
    ((text slide) hint-text))
   ((pair? slide)
    (slides->sxml slide hint-text))
   (else (slide hint-text))))

(define (slides->sxml slide-list initial-hint)
  ;; Fold over the slide list in reverse, passing the hint string forward
  (let* ((final-slides '())
         (final-hint
          (fold (lambda (slide prev-hint)
                  (let-values (((slide-sxml hint-text)
                                (slide->sxml slide prev-hint)))
                    (set! final-slides (cons slide-sxml final-slides))
                    hint-text))
                initial-hint
                (reverse slide-list))))
    (values final-slides final-hint)))

(define (slides . slide-list)
  (let-values (((slides-sxml _)
                (slides->sxml slide-list "EOF")))
    (with-output-to-string
      (lambda ()
        (sxml->xml `(html (@ (lang "en"))
                          (head
                           (meta (@ (charset "utf-8")))
                           (meta (@ (name "viewport")
                                    (content "width=device-width, initial-scale=1, shrink-to-fit=no")))
                           (link (@ (rel "stylesheet") (href "slides.css"))))
                          (body ,slides-sxml)))))))

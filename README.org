#+title: schtage - Generate minimal presentation slides

* Usage

For now, just clone the repo and use Guix:

#+begin_src sh

  guix shell -m manifest.scm -- ./schtage -i your-presentation.scm -o your-presentation.pdf

#+end_src

* License

This project is licensed under the [[file:LICENSE][GNU General Public License v3]].
